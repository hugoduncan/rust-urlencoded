//! Serialisation support for url encoded structs

use std::fmt;
use std::num::FpCategory;
use std::io::Write;

use num::Float;
use serde::{Deserializer, Serialize};
use serde::ser;

use url::percent_encoding::{percent_encode, FORM_URLENCODED_ENCODE_SET};

use ::error::*;

struct Serializer<'a, W: 'a + Write> {
    writer: &'a mut W,
    key: Option<String>,
    first: bool,
    elided_first: bool
}

impl<'a, W> fmt::Debug for Serializer<'a, W> where W: 'a + Write {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        try!(write!(f, "Serializer {{ ",));
        try!(write!(f, "key: {:?}, ", self.key));
        try!(write!(f, "first: {:?}, ", self.first));
        try!(write!(f, "elided_first: {:?} ", self.elided_first));
        write!(f, "}}")
    }
}

impl<'a, W: Write> Serializer<'a, W> {
    fn new(w: &mut W) -> Serializer<W> {
        trace!("Serializer::new");
        Serializer {
            writer: w,
            key: None,
            first: false,
            elided_first: false
        }
    }
}

impl<'a, W: Write> ser::Serializer for Serializer<'a, W> {
    type Error = Error;

    #[inline]
    fn visit_unit(&mut self) -> Result<(), Error> {
        try!(self.writer.write("null".as_bytes()));
        Ok(())
    }

    #[inline]
    fn visit_bool(&mut self, value: bool) -> Result<(), Error> {
        if value {
            try!(self.writer.write("true".as_bytes()));
        } else {
            try!(self.writer.write("false".as_bytes()));
        }
        Ok(())
    }

    #[inline]
    fn visit_isize(&mut self, value: isize) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_i8(&mut self, value: i8) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_i16(&mut self, value: i16) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_i32(&mut self, value: i32) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_i64(&mut self, value: i64) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_usize(&mut self, value: usize) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_u8(&mut self, value: u8) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_u16(&mut self, value: u16) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_u32(&mut self, value: u32) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_u64(&mut self, value: u64) -> Result<(), Error> {
        Ok(try!(write!(self.writer, "{}", value)))
    }

    #[inline]
    fn visit_f64(&mut self, value: f64) -> Result<(), Error> {
        fmt_f64_or_null(self.writer, value)
    }

    #[inline]
    fn visit_char(&mut self, v: char) -> Result<(), Error> {
        escape_char(self.writer, v)
    }

    #[inline]
    fn visit_str(&mut self, value: &str) -> Result<(), Error> {
        escape_str(self.writer, value)
    }

    #[inline]
    fn visit_none(&mut self) -> Result<(), Error> {
        Err(Error::SuppressNone)
    }

    #[inline]
    fn visit_some<V>(&mut self, value: V) -> Result<(), Error>
        where V: Serialize
    {
        value.serialize(self)
    }

    #[inline]
    fn visit_seq<V>(&mut self, mut visitor: V) -> Result<(), Error>
        where V: ser::SeqVisitor,
    {
        trace!("visit_seq {:?}", self);
        self.first = true;
        while let Some(()) = try!(visitor.visit(self)) { }
        Ok(())
    }

    #[inline]
    fn visit_seq_elt<T>(&mut self, value: T) -> Result<(), Error>
        where T: Serialize,
    {
        trace!("visit_seq_elt {:?}", self);
        let first = self.first;
        self.first = false;

        if !first {
            try!(self.writer.write("&".as_bytes()));
            try!(self.writer.write_fmt(
                format_args!("{}",
                             self.key.as_ref()
                             .map(|s| &s[..])
                             .unwrap_or("Nokey"))));
            try!(self.writer.write("=".as_bytes()));
        }

        value.serialize(self)
    }

    #[inline]
    fn visit_map<V>(&mut self, mut visitor: V) -> Result<(), Error>
        where V: ser::MapVisitor,
    {
        trace!("visit_map {:?}", self);
        self.first = true;
        while let Some(()) = try!(visitor.visit(self)) { }
        Ok(())
    }

    #[inline]
    fn visit_map_elt<K, V>(&mut self, key: K, value: V) -> Result<(), Error>
        where K: Serialize, V: Serialize,
    {
        trace!("visit_map_elt {:?}", self);
        if self.first {
            self.elided_first = false;
        }

        if !self.first && !self.elided_first {
            try!(self.writer.write("&".as_bytes()));
        }

        // let first = self.first;
        self.first = false;

        let k = to_string(&key).unwrap();
        trace!("visit_map_elt {:?}", k);
        self.key = Some(k);
        trace!("visit_map_elt with key {:?}", self);

        match to_string(&value) {
            Ok(_) => {
                trace!("visit_map_elt case 1");
                self.elided_first = false;
                try!(key.serialize(self));
                try!(self.writer.write("=".as_bytes()));
                trace!("visit_map_elt serialize value");
                let v = try!(value.serialize(self));
                self.first = false;
                Ok(v)
            },
            Err(Error::SuppressNone) => {
                trace!("visit_map_elt case 2");
                self.elided_first = true;
                self.first = false;
                Ok(())
            },
            Err(e) => Err(e)
        }
    }
}

fn fmt_f64_or_null<W: Write>(wr: &mut W, value: f64) -> Result<(), Error> {
    match value.classify() {
        FpCategory::Nan | FpCategory::Infinite => {
            try!(wr.write("null".as_bytes()));
        },
        _ => {
            // try!(wr.write(&f64::to_str_digits(value, 6).as_bytes()))
            try!(write!(wr, "{}", value));
        },
    };
    Ok(())
}

#[inline]
pub fn escape_str<W: Write>(wr: &mut W, value: &str) -> Result<(), Error> {
    try!(wr.write(&percent_encode(value.as_bytes(),
                                  FORM_URLENCODED_ENCODE_SET).as_bytes()));
    Ok(())
}

#[inline]
pub fn escape_char<W: Write>(wr: &mut W, value: char) -> Result<(), Error> {
    let mut buf = &mut [0; 4];
    value.encode_utf8(buf);
    try!(wr.write(&percent_encode(buf, FORM_URLENCODED_ENCODE_SET).as_bytes()));
    Ok(())
}

#[inline]
pub fn to_writer<W, T>(wr: &mut W, value: &T) -> Result<(), Error>
    where W: Write,
          T: Serialize,
{
    let mut ser = Serializer::new(wr.by_ref());
    try!(value.serialize(&mut ser));
    Ok(())
}

#[inline]
pub fn to_vec<T>(value: &T) -> Result<Vec<u8>, Error>
    where T: Serialize,
{
    let mut wr = Vec::with_capacity(128);
    try!(to_writer(&mut wr, value));
    Ok(wr)
}

#[inline]
pub fn to_string<T>(value: &T) -> Result<String, Error>
    where T: Serialize,
{
    let vec = try!(to_vec(value));
    let s = try!(String::from_utf8(vec));
    Ok(s)
}

#[cfg(test)]
mod test {
    use super::*;
    use env_logger;

    #[derive(Serialize, Deserialize)]
    struct Data {
        foo: Vec<String>,
        v: i64
    }

    #[test]
    fn test_form_urlencoded() {
        let _log = env_logger::init();
        let data1 = Data {
            foo: vec!["é&".to_string(),"".to_string(), "#".to_string ()],
            v: 1
        };
        let encoded = to_string(&data1).unwrap();
        assert_eq!("foo=%C3%A9%26&foo=&foo=%23&v=1".to_string(), encoded);
    }

    #[derive(Serialize, Deserialize)]
    struct OptionData {
        foo: Option<String>,
        v: Option<i64>
    }

    #[test]
    fn test_form_urlencoded_option() {
        let _log = env_logger::init();
        let data1 = OptionData {
            foo: None,
            v: Some(1)
        };
        let encoded = to_string(&data1).unwrap();
        assert_eq!("v=1".to_string(), encoded);
    }

    #[derive(Serialize, Deserialize)]
    struct VecData {
        foo: Option<String>,
        v: Option<i64>,
        ids: Vec<String>,
        ns: Vec<u32>,
    }

    #[test]
    fn test_form_urlencoded_vec() {
        let _log = env_logger::init();
        let data1 = VecData {
            foo: None,
            v: Some(1),
            ids: vec![],
            ns: vec![],
        };
        let encoded = to_string(&data1).unwrap();
        assert_eq!("v=1&ids=&ns=".to_string(), encoded);
    }


    #[derive(Debug, Serialize, Deserialize)]
    pub struct CreateVpcRequest {
        #[serde(rename="CidrBlock")]
        pub cidr_block: String,
        #[serde(rename="dryRun")]
        pub dry_run: Option<bool>,
        #[serde(rename="instanceTenancy")]
        pub instance_tenancy: Option<String>,
    }

    #[test]
    fn test_form_urlencoded_option2() {
        let _log = env_logger::init();
        let data1 = CreateVpcRequest {
            cidr_block: "block".into(),
            dry_run: None,
            instance_tenancy: Some("Default".into())
        };
        let encoded = to_string(&data1).unwrap();
        assert_eq!("CidrBlock=block&instanceTenancy=Default".to_string(),
                   encoded);
    }

}
