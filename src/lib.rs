#![feature(unicode)]
#![feature(custom_attribute, custom_derive)] // Not sure why we need this globally
#![cfg_attr(test, feature(custom_attribute, custom_derive, plugin))]
#![cfg_attr(test, plugin(serde_macros))]
#![cfg_attr(test, deny(warnings))]

#[macro_use] extern crate log;
extern crate serde;
extern crate url;
#[cfg(test)] extern crate env_logger;
extern crate num;

pub use ::error::*;
pub use ::urlencoded::*;

mod error;
mod urlencoded;
