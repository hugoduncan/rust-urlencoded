//! Error type for urlencoded

use std::error;
use std::fmt;
use std::io;
use std::string::FromUtf8Error;

/// Urlencoded Error
#[derive(Debug)]
pub enum Error {
    /// Internal error value
    SuppressNone,
    /// An IO error
    IoError(io::Error),
    /// A utf error
    FromUtf8Error(FromUtf8Error)
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IoError(err)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(err: FromUtf8Error) -> Error {
        Error::FromUtf8Error(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            Error::SuppressNone => write!(f,"SuppressNone"),
            Error::IoError(ref e) => write!(f,"{}",e),
            Error::FromUtf8Error(ref e) => write!(f, "{}", e),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::SuppressNone => "Internal Error",
            Error::IoError(ref e) => error::Error::description(e),
            Error::FromUtf8Error(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::SuppressNone => None,
            Error::IoError(ref e) => Some(e),
            Error::FromUtf8Error(ref e) => Some(e),
        }
    }
}
